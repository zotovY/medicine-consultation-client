import SettingsPage from "./pages/settings";
import SettingsAccountPage from "./pages/account";
import SettingsConsultationPage from "./pages/consultations";
import SettingsReviewPage from "./pages/reviews";
import SettingsNotificationPage from "./pages/notification";
import SettingsPasswordPage from "./pages/password";
import SettingsLinksPage from "./pages/links";
import SettingsDoctorPage from "./pages/doctor";
import SupportPage from "./pages/support";

export {
    SettingsPage,
    SettingsAccountPage,
    SettingsConsultationPage,
    SettingsReviewPage,
    SettingsNotificationPage,
    SettingsPasswordPage,
    SettingsLinksPage,
    SettingsDoctorPage,
    SupportPage
}

