import ChooseDoctor from "./pages/find-doctor";
import DetailDoctorPage from './pages/detail';
import FilterCityModal from './components/filter/city-add-modal';
import SymptomsPage from './pages/symptoms';

export {
    ChooseDoctor,
    FilterCityModal,
    DetailDoctorPage,
    SymptomsPage,
}