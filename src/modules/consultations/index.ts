import AppointmentPage from "./pages/appointment";
import ConsultationPage from "./pages/consultation";
import HubConsultationPage from "./pages/hub/index";
export { AppointmentPage, ConsultationPage, HubConsultationPage };
